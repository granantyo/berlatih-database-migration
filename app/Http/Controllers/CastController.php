<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function index() 
    {
        $casts = DB::table('cast')->get(); //select * from cast;
        //dd($casts);
        return view('cast.index', compact('casts'));
    }

    public function create() 
    {
        return view('cast.create');
    }

    public function store(Request $request) 
    {
        $request->validate([
            'nama'=> 'required|unique:cast',
            'umur'=> 'required',
            'bio'=> 'required'
        ]);
        //insert into cast (nama, umur, bio) values('request[nama]','request[umur]','request[bio]')
        $query = DB::table('cast')->insert([ 
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast');
    }

    public function show($id) 
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.show', compact('cast'));
    }

    public function edit($id) 
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request) 
    {
        $request->validate([
            'nama'=> 'required|unique:cast',
            'umur'=> 'required',
            'bio'=> 'required'
        ]);
        //insert into cast (nama, umur, bio) values('request[nama]','request[umur]','request[bio]')
        $query = DB::table('cast')->where ('id', $id)->update([ 
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast');
    }

    public function destroy($id) 
    {
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }


    public function welcomee(Request $request)
    {
        $first = $request->first_name;
        $last = $request->last_name;
        //dd($first,$last);
        return view('welcomee', compact('first', 'last'));
    }
}
