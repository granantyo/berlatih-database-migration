@extends('layouts.master')

@section('title')
    <h2>Show Cast {{$cast->id}}</h2>
@endsection

@section('content')
    <h4>Nama: {{$cast->nama}}</h4>
    <p>Umur: {{$cast->umur}}</p>
    <p>Biodata: {{$cast->bio}}</p>
@endsection