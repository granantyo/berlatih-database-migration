@extends('layouts.master')

@section('title')
    Edit Cast {{$cast->id}}
@endsection

@section('content')
    <div>
        <form action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Nama</label>
                <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="body">Umur</label>
                <input type="text" class="form-control" name="umur" value="{{$cast->umur}}"placeholder="Masukkan Umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="body">Biodata</label>
                <input type="text" class="form-control" name="bio" value="{{$cast->bio}}"placeholder="Masukkan Biodata">
                @error('bio')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
@endsection